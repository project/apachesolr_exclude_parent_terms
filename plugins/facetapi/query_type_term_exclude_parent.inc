<?php

/**
 * @file
 * Term query type plugin for the Apache Solr adapter.
 */

/**
 * Plugin for "term" query types.
 */
class ApacheSolrFacetapiTermExcludeParent extends ApacheSolrFacetapiTerm implements FacetapiQueryTypeInterface {

  /**
   * Returns the query type associated with the plugin.
   *
   * @return string
   *   The query type.
   */
  static public function getType() {
    return 'term_exclude_parent';
  }

  /**
   * Adds the filter to the query object.
   *
   * @param DrupalSolrQueryInterface $query
   *   An object containing the query in the backend's native API.
   */
  public function execute($query) {

    $settings = $this->adapter->getFacet($this->facet)->getSettings();
    // Adds the operator parameter.
    $operator = $settings->settings['operator'];
    $ex = (FACETAPI_OPERATOR_OR != $operator) ? '' : "{!ex={$this->facet['field']}}";
    $query->addParam('facet.field', $ex . $this->facet['field']);

    if (!empty($settings->settings['facet_missing'])) {
      $query->addParam('f.' . $this->facet['field'] . '.facet.missing', 'true');
    }
    // Adds "hard limit" parameter to prevent too many return values.
    $limit = empty($settings->settings['hard_limit']) ? 20 : (int) $settings->settings['hard_limit'];
    $query->addParam('f.' . $this->facet['field'] . '.facet.limit', $limit);

    // Adds "facet mincount" parameter to limit the number of facets.
    if (isset($settings->settings['facet_mincount'])) {
      $count = $settings->settings['facet_mincount'];
      $query->addParam('f.' . $this->facet['field'] . '.facet.mincount', $count);
    }

    $active = $this->adapter->getActiveItems($this->facet);
    // Remove the all tids that are parents of any tid in the list
    $this->remove_parents($active);

    // Adds filter based on the operator.
    if (FACETAPI_OPERATOR_OR != $operator) {

      foreach ($active as $value => $item) {
        // Handle facet missing:
        if ($value == '_empty_' && !empty($settings->settings['facet_missing'])) {
          $query->addFilter($this->facet['field'], '[* TO *]', TRUE);
        }
        else {
          $query->addFilter($this->facet['field'], $value);
        }
      }
    }
    else {
      // OR facet.
      $local = "tag={$this->facet['field']}";
      $values = array_keys($active);
      if ($values) {
        $query->addFilter($this->facet['field'], '(' . implode(' OR ', $values) . ')', FALSE, $local);
      }
    }
  }

  /**
   * Removes parents from a list of terms. Only works for a level 1 taxonomy
   * This is not meant to handle a unlimited depth taxonomy
   *
   * @param array $terms
   */
  public function remove_parents(&$terms) {
    $parents = array();
    foreach ($terms as $term_id => $term) {
      $parents = taxonomy_get_parents($term_id);
      foreach ($parents as $parent) {
        unset($terms[$parent->tid]);
      }
    }
  }
}
